# learnjs

in this repo I collect little bits and pieces I've written to learn how javascript works. Most content so far revolves around promises (and specifically their error handling) because that is what I needed in my current project. 

# setup
No setup necessary if you only want to run the scripts (except having node installed on your laptop).

For example from root:

```bash
node promises/scripts/00_error_from_promise.js
```

I've also added a test file under `promises/test` to see whether this format works better for me but I think I prefer the simple scripts. :blush:

If you want to run the test file, you need to install jest. You can do so by using:

```
yarn install
```

You can then run: 

```js
yarn test
```
