promise_fun = require("../promise_fun");

const funWithErrorInThen = () => {
  // we need to return to return the promise
  // if we catch the error here and just log it, the next then
  // calls in the function chain will be executed because
  // everything was handled here.
  return promise_fun.myPromiseFunc(false, true).then(isUnwanted => {
    if (isUnwanted) {
      return Promise.reject(Error("something bad during then"));
    }
  });
};

// function chain
// because we did not handle the error in the function,
// the function will return a rejected promise -> we go to catch here.
funWithErrorInThen()
  .then(() => console.log("this is not executed."))
  .catch(error => console.log("an error in the chain", error));
