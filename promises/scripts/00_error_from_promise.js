promise_fun = require("../promise_fun")

promise_fun
        .myPromiseFunc((shouldError = true), (isUnwantedRetValue = false)) // this promise rejects
        .then(() => {
          // this is not executed because the promise is rejected
          console.log("this is not executed");
        })
        .then(() => console.log("this is not executed."))
        .catch(err => console.log(err))