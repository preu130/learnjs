promise_fun = require("../promise_fun");

const funWithoutError = () => {
  // this promise resolves because return value is not unwanted (second argument = false)
  return promise_fun.myPromiseFunc(false, false).then(isUnwanted => {
    if (isUnwanted) {
      return Promise.reject(Error("something bad during then"));
    }
  });
};


const funWithErrorInThen = () => {
  // we return the rejected promise and do not have a catch.
  // if we catch the error here and just log it, the next then
  // calls in the function chain will be executed because
  // everything was handled here.
  return promise_fun.myPromiseFunc(false, true).then(isUnwanted => {
    if (isUnwanted) {
      return Promise.reject(Error("something bad during then"));
    }
  });
};


funWithoutError() // resolved promise
  .then(() => {
    console.log("this is executed"); // we log this
  })
  .then(() => funWithErrorInThen()) // rejected promise 
  .then(() => console.log("this is not executed.")) // skipped because rejected promise is caught in catch
  .catch(error => console.log("an error in the chain", error));
